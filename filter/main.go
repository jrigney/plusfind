package main

import "fmt"
import "strings"

func main() {
	fmt.Println("vim-go")
	in := []string{"bob", "", "mary"}
	out := Selected(in, func(s string) bool {
		return s != ""
	})

	fmt.Println(strings.Join(out, ","))
}

func Selected(s []string, fn func(string) bool) []string {
	var selected []string // == nil
	for _, v := range s {
		if fn(v) {
			selected = append(selected, v)
		}
	}
	return selected
}
