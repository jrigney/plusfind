package main

import "testing"
import "reflect"

func TestSelected(t *testing.T) {
	in := []string{"bob", "", "mary"}
	actual := Selected(in, func(s string) bool {
		return s != ""
	})

	expected := []string{"bob", "mary"}

	if !reflect.DeepEqual(actual, expected) {
		t.Fail()
	}

}
