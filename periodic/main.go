package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {

	for range time.NewTicker(2 * time.Second).C {
		version := getIpAddress()
		fmt.Println("ip:", version.Ip)
	}

}
func getIpAddress() ipAddress {

	responseReader, err := http.Get("http://ip.jsontest.com")
	if err != nil {
		log.Fatal("could not connect")
	}
	defer responseReader.Body.Close()

	response, err := ioutil.ReadAll(responseReader.Body) //Body is a io.ReadCloser
	if err != nil {
		log.Fatal("could not read response")
	}

	fmt.Println("resp as string", string(response))
	fmt.Printf("resp Printf  %s\n", response) //notice we don't need string(response) any more
	fmt.Println("err", err)

	var ipAddress ipAddress
	json.Unmarshal(response, &ipAddress)

	return ipAddress
}

type ipAddress struct {
	Ip string `json:"ip"`
}
