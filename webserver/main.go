package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func rootHandler(respW http.ResponseWriter, r *http.Request) {
	respW.Header().Set("Content-Type", "text/html")
	respW.WriteHeader(http.StatusOK)
	data, err := ioutil.ReadFile("index.html")
	if err != nil {
		panic(err)
	}
	respW.Header().Set("Content-Length", fmt.Sprint(len(data)))
	fmt.Fprint(respW, string(data))
}

func main() {
	fmt.Println("vim-go")
	http.HandleFunc("/", rootHandler)
	log.Fatal(http.ListenAndServe(":1776", nil))
}
